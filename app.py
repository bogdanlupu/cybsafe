import logging

from flask import Flask

from config import Config
from db import db

logging.basicConfig(level=logging.DEBUG)

app = Flask(__name__)
app.config.from_object(Config)

db.init_app(app)


with app.app_context():
    from application.models.stats import Stat
    from application.models.pokemon import Pokemon, PokemonStat
    db.create_all()
    db.session.commit()
    from application.controllers.pokemon_controller import pokemon_controller

@app.route("/")
def hello_world():
    return {
        'message': 'Hello World!'
    }


app.register_blueprint(pokemon_controller, url_prefix='/pokemon')


if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')
