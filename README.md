# Pokemon Crawler

## Description
This service integrates with [PokeAPI](http://https://pokeapi.co/) to provide information about Pokemon.


## Flow
Queries are executed based on the external ID. If an entry is present in the database and it is not too old, it is fetched.
Otherwise, the external service is queried, the results are stored and then served to the client.

## Update cronjob
###### _This feature is WIP_
The service can automatically refresh its data at a given interval. To do so, a cronjob is set up which can be executed 
at the desired rate (recommended is every 24h). If desired, this job can also be triggered manually. It is run as a background
task, therefore an HTTP response is issued immediately.