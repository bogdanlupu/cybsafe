from ..models.pokemon import Pokemon


class PokemonDto:
    def __init__(self, id, name, height, weight, created, updated, stats):
        self.id = id
        self.name = name
        self.height = height
        self.weight = weight
        self.created = created
        self.updated = updated
        self.stats = stats

    @staticmethod
    def from_model(pokemon: Pokemon):
        return PokemonDto(pokemon.id, pokemon.name, pokemon.height, pokemon.weight, None, None, pokemon.stats)
