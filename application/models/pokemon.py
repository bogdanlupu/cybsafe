from sqlalchemy import String, Column, Integer, TIMESTAMP, func, ForeignKey
from sqlalchemy.orm import relationship

from db import db


class Pokemon(db.Model):
    __tablename__ = 'pokemon'
    id = Column(Integer, primary_key=True)
    external_id = Column(Integer, unique=True, nullable=False, index=True)
    name = Column(String, unique=True, nullable=False)
    height = Column(Integer, nullable=False)
    weight = Column(Integer, nullable=False)
    created = Column(TIMESTAMP(timezone=False), nullable=False, default=func.now())
    updated = Column(TIMESTAMP(timezone=False), nullable=False, default=func.now())
    stats = relationship('Stat', secondary='pokemon_stats')


class PokemonStat(db.Model):
    __tablename__ = 'pokemon_stats'
    pokemon_id = Column(Integer, ForeignKey('pokemon.id'), primary_key=True)
    stat_id = Column(Integer, ForeignKey('stats.id'), primary_key=True)
    value = Column(Integer, nullable=False)
