from sqlalchemy import String, Column, Integer, TIMESTAMP, func
from sqlalchemy.orm import relationship

from db import db


class Stat(db.Model):
    __tablename__ = 'stats'
    id = Column(Integer, primary_key=True)
    external_id = Column(Integer, unique=True, nullable=False, index=True)
    name = Column(String, unique=True, nullable=False)
    stats = relationship('Pokemon', secondary='pokemon_stats')
