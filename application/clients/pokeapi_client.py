import asyncio
import logging
from http import HTTPStatus
from typing import Optional, List

from aiohttp import ClientSession

from flask import current_app as app

logger = logging.getLogger(__name__)


class PokeapiClient:
    BASE_URL = app.config['POKEAPI_BASE_URL']

    async def get_all(self):
        async with ClientSession(self.BASE_URL) as session:
            async with session.get('/api/v2/pokemon') as response:
                if response.status != HTTPStatus.OK:
                    logger.error(f'PokeAPI response status was {response.status}.')
                    response.raise_for_status()
                count = (await response.json()).get('count')
            async with session.get('/api/v2/pokemon', params={'limit': count}) as response:
                if response.status != HTTPStatus.OK:
                    logger.error(f'PokeAPI response status was {response.status}.')
                    response.raise_for_status()
                return await response.json()

    async def get(self,
                  id: Optional[int] = None, ids: Optional[List[int]] = None, session: Optional[ClientSession] = None):
        assert bool(id) ^ bool(ids), f'Exactly one of `id` and `ids` is expected, but got id {id}, ids {ids}'
        if ids:
            async with ClientSession(self.BASE_URL) as session:
                coroutines = [self.get(id=id, session=session) for id in ids]
                return await asyncio.gather(*coroutines)
        if id:
            logger.debug(f'Getting pokemon with id {id}')
            if not session:
                async with ClientSession(self.BASE_URL) as session:
                    async with session.get(f'/api/v2/pokemon/{id}') as response:
                        return await response.json()
            else:
                async with session.get(f'/api/v2/pokemon/{id}') as response:
                    return await response.json()
