from application.models.pokemon import Pokemon
from db import db


class PokemonRepository:
    async def get(self, external_id):
        return Pokemon.query.filter_by(external_id=external_id).first()

    def create(self, pokemon):
        db.session.add(pokemon)
        db.session.commit()
        return pokemon
