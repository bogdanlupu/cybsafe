import logging

from ..clients.pokeapi_client import PokeapiClient
from ..models.pokemon import Pokemon
from ..repositories.pokemon_repository import PokemonRepository

logger = logging.getLogger(__name__)


class PokemonService:
    def __init__(self):
        self.pokemon_client = PokeapiClient()
        self.pokemon_repository = PokemonRepository()

    async def update_entries(self):
        entries = await self.pokemon_client.get_all()
        ids = [entry['url'].split('/')[-2] for entry in entries['results']]
        logger.info(f'Updating {len(ids)} entries')
        complete_entries = await self.pokemon_client.get(ids=ids)
        print(complete_entries)

    async def get_all(self):
        return await self.pokemon_client.get_all()

    async def get(self, id):
        result = await self.pokemon_repository.get(id)
        if not result:
            logger.info('Entry not found in database, querying service...')
            result = await self.pokemon_client.get(id=id)
            return self.create(result)
        return result

    def create(self, result: dict):
        pokemon = Pokemon(external_id=result['id'],
                          name=result['name'],
                          height=result['height'],
                          weight=result['weight'])
        return self.pokemon_repository.create(pokemon)
