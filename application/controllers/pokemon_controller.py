import asyncio
import json

from flask import Blueprint

from ..dto.pokemon_dto import PokemonDto
from ..services.pokemon_service import PokemonService

pokemon_controller = Blueprint('pokemon_controller', __name__)

pokemon_service = PokemonService()


@pokemon_controller.get('/')
async def get_all():
    result = await pokemon_service.get_all()
    return result


@pokemon_controller.get('/<id>')
async def get(id: int):
    return PokemonDto.from_model(await pokemon_service.get(id)).__dict__


@pokemon_controller.get('/update')
async def update_entries():
    await pokemon_service.update_entries()
    # task = asyncio.create_task(pokemon_service.update_entries())
    return {'message': 'Job started successfully'}
