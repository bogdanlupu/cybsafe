FROM python:3
ENV PYTHONUNBUFFERED=1
WORKDIR /code
COPY requirements.txt /code/
RUN pip install -r requirements.txt
COPY . /code/
EXPOSE 5000
ENV FLASK_APP=app:app
CMD ["flask", "run", "--host", "0.0.0.0"]